shuffle = (a) ->
  j = 0 
  x = 0
  for i in [a.length - 1..1]
    j = Math.floor(Math.random() * (i + 1))
    x = a[i]
    a[i] = a[j]
    a[j] = x
  a

clips = shuffle ["cashier", "luggages", "packages", "road", "pigeons", "swing", "water", "subway", "box", "slide"]
langsdefault = ["english", "italian", "hungarian"]
langs = shuffle [
    {english : "english",italian:"inglese", hungarian:"angol", loading: "loading"}, 
    {english : "italian",italian:"italiano", hungarian:"olasz", loading : "caricamento"}, 
    {english : "hungarian",italian:"ungherese", hungarian:"magyar", loading: "betöltés"} 
]    

loadedfiles = {}

loadingpercent = () ->
  fs = Object.keys(loadedfiles)
  a = 0
  for f in fs
    if loadedfiles[f] then a++
  a / fs.length

class v2
  constructor:(@x, @y) ->

tracks = {}
current = undefined
infoactivateing = true
dragoffset = new v2(0,0)

nothing = false
autopilot_allowed = true
lasttrioffset = new v2(0,0)
lastuseraction = 0
mousepos = new v2(0,0)
fakecursortimeout = 30000

dist = (a, b) -> Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2))
clamp = (a, b, p) -> if p < a then a else if p > b then b else p
lerp = (a, b, t) -> a*(1-t)+b*t
subv2 = (a, b) -> new v2(a.x - b.x, a.y - b.y)
ilerp = (a, b, p) -> (p - a) / (b - a)
random = () -> -1 + Math.random() * 2
randomfrom = (ar) -> if ar.length > 0 then ar[Math.round(Math.random() * (ar.length - 1))] else undefined
randomease = () -> randomfrom ["linear", "swing"]#, "easeInOutQuad","easeInOutSine", "easeOutCubic", "easeOutQuint", "easeOutQuad"]

tri_clamp = (a, w, h, p) ->
  p = new v2(clamp(a.x, a.x + w, p.x), clamp(a.y, a.y + h, p.y))
  if p.x < a.x + w * 0.5
    p.x = clamp(lerp(a.x, a.x + w * 0.5, ilerp(a.y, a.y + h, p.y)), clamp(a.x, a.x + w * 0.5, p.x), p.x)
    p.y = clamp(a.y, a.y + h, p.y)
  else
    p.x = clamp(clamp(a.x + w * 0.5, a.x + w, p.x), lerp(a.x + w * 0.5, a.x + w, ilerp(a.y + h, a.y,  p.y)), p.x)
    p.y = clamp(a.y, a.y + h, p.y)
  p

tri = () ->
  tr = $("#control").offset()
  [
    new v2(tr.left - 130, tr.top),
    new v2(tr.left + 130, tr.top),
    new v2(tr.left, tr.top + 210)
  ]

getvolumes = (p) ->
  triangle = tri()
  for t in triangle
    t.x += $("#control").width() * 0.5
  ds = []
  for t in triangle
    ds.push (1 - clamp(0, 1, ilerp(0, 210, dist(p, t))))
  r = []
  max = ds.reduce((a, v) -> if v > a then v else a)
  for d in ds
    r.push d / max
  r


nothing_is_in = (combo) ->
  trgs = [new v2(0,0), new v2(1,0), new v2(0.5, 1), new v2(0.25, 0.5), new v2(0.5, 0), new v2(0.75, 0.5), new v2(0.375, 0.25), new v2(0.625, 0.25), new v2(0.5, 0.5), new v2(0.5, 0.2625)]
  normps = combo.map((v) -> new v2(ilerp(0, 260, v.x), ilerp(0, 210, v.y)))
  misplaced = normps.length
  bias = 0.042
  for k in clips
    $("##{tracks[k].name}_knob").css("background-color", "")    
  for p, pi in normps
    matched = false
    for tp, i in trgs
      if not matched 
        if typeof tp isnt "string" and dist(tp, p) < bias
          matched = true
          trgs[i] = tracks[clips[pi]].name
          misplaced--
  for t, i in trgs
    if typeof t is "string"
      $("##{t}_knob").css("background-color", "#222")
  misplaced is 0

do_nothing = () ->
  autopilot_allowed = false
  nothing = true
  $("#info").css("display", "block")
  $("#top, #bottom").animate({opacity:0.0}, 1000)
  $("body, html").css("background-color", "#000")
  $("#tri").css("fill", "#111")
  console.log "doing nothing"
  for k in clips
    tracks[k].toggle k
    tracks[k].sounds.setvolumes([0,0,0])
    $("##{k}_knob").css("display", "none")
  tracks.nothing.position = new v2($("#control").offset().left + $("#control").width() * 0.5, 105 + $("#control").offset().top - $("#control").height() * 0.125)
  tracks.nothing.toggle "nothing"

set_volume = (x, y) ->
  tr = $("#control").offset()
  p = tri_clamp(new v2(tr.left, tr.top), 260, 210, new v2(x, y))
  volumes = getvolumes p
  tracks[current].sounds.setvolumes(volumes)
  $("##{current}_knob").css({left:"#{p.x}px", top:"#{p.y}px"})
  tracks[current].position = p
  if clips.reduce(((a, k) -> if not tracks[k].playing then false else a), true)
    combo = clips.map((k) -> subv2(tracks[k].position, lasttrioffset))
    if not nothing and nothing_is_in combo then do_nothing()

requestmedia = (n, format) ->
  req = new XMLHttpRequest()
  req.open('GET', "media/#{n}.#{format}", true)
  req.responseType = "blob"
  req.onload = () ->
    if this.status is 200
      blob = this.response
      console.log "#{n}.#{format} finished loading"
      loadedfiles[n] = true
      $("##{n}").attr("src", (URL.createObjectURL blob))
  req.send()

class sounds
  players : []
  name : ""
  loaded:false
  constructor:(n) ->
    @name = n
    @players = []
    for l in langs
      t = "#{n}_#{l.english[0]}"
      s = $("<audio>", {id : t})
      $("body").append s
      # $(t).attr("preload", "auto")
      requestmedia(t, "mp3")
      @players.push $("##{t}")
  toggle : (playing) ->
    if not @loaded then @loaded = true
    for p in @players
      p.get(0).currentTime = $("##{@name}").get(0).currentTime
      if playing
        p.get(0).play()
      else 
        p.get(0).pause()
  setvolumes : (vs) ->
    for p, i in @players
      p.get(0).volume = vs[i]

grabknob = (k, n) ->
  # p = new v2($(k).offset().left, $(k).offset().top)
  # mouse = new v2(e.clientX, e.clientY)
  # dragoffset = new v2(mouse.x - p.x - 25, mouse.y - p.y - 25)
  $(k).addClass("dragged")
  current = n

dragknob = (x,y) ->
  if current isnt undefined
    set_volume(x, y)

dropknob = () ->
  $("##{current}_knob").removeClass("dragged")
  current = undefined

class track
  video : {}
  sounds : []
  loaded : false
  name : ""
  position : new v2(0,0)
  playing : false
  restart : () ->
    if not @loaded then @loaded = true
    @video.get(0).currentTime = 0
    @video.get(0).play()
    @sounds.toggle true
  createknob : (p, n, knob) ->
    k = $("<div>", {id:"#{@name}_knob", class:"knob", style:""})
    style = 
      "border-radius":"0%" 
      left: p.left + $("##{n}").width() *0.5 + "px"
      top:p.top + $("##{n}").height() *0.5 + "px" 
      opacity:0.4
      width: "#{100/clips.length}%"
      height:$("##{n}").height() + "px"
    $("#control-container").append k
    $(knob).css style
    $(knob).mouseenter(() -> 
      $("##{n}").css("filter", "brightness(130%)")
      window.setTimeout((()->$("##{n}").css({"filter":"brightness(100%)"})), 200)
    )
    $(knob).mousedown((e) -> grabknob(knob, n))
    $(knob).mouseup(() -> dropknob())
    $(knob).animate({opacity:"0.8",left:"#{tracks[n].position.x}px", width:"50px", height:"50px",top:"#{tracks[n].position.y}px", "border-radius":"50%"}, 500)
    $("##{@name}").addClass("playing")
  toggle : (n) ->
    @playing = not @playing
    knob = "##{n}_knob"
    if @playing 
      @video.get(0).play()
      @createknob(@video.offset(), @name, knob)
      @sounds.setvolumes(getvolumes(@position))
    else
      $("##{@name}").removeClass("playing")
      $("##{@name}_knob").animate({opacity:"0.0"}, 300, () -> $(knob).get(0).remove())
      if $(".knob").get(0) is undefined then current = undefined
      @video.get(0).pause()
    @sounds.toggle @playing
  pick_pos : () ->
    nt = new v2(130 + $("#control").offset().left + (130 * random()), $("#control").offset().top + (105 * random()) )
    nt = tri_clamp(new v2($("#control").offset().left, $("#control").offset().top), 260, 210, nt)
    @position = new v2( nt.x,  nt.y)
  constructor:(n, x, w, parent) ->
    @pick_pos()
    v = $("<video>", {id: n, style : "left : #{x}%; width:#{w}%;" + (if parent is "#bottom" then "bottom:0" else "")})
    $(parent).append v
    # $("##{n}").attr("preload", "auto")
    # $("##{n}")[0].autoplay = true
    requestmedia(n, "mp4")
    @video = $("##{n}")
    @video.bind("ended", () -> tracks[n].restart())
    @video.click(() -> tracks[n].toggle(n))
    @video.get(0).volume = 0
    @sounds = new sounds n
    @sounds.setvolumes([0,0,0])
    @name = n

infotoggle = () ->
  infoshowing = not infoshowing
  # $("#info").css({"opacity":(if infoshowing then 1 else 0)})
  # if infoshowing then $("#info").css("display", "block")
  # else
    # window.setTimeout((() -> $("#info").css("display","none")), 500)

getloaded = () ->
  ls = []
  for c in clips
    ls.push tracks[c].loaded
    ls.push tracks[c].sounds.loaded
  ls.reduce(((a, v) -> if v then a+1 else a), 0)


resize = () ->
  console.log "resizing"
  newtrioffset = $("#control").offset()
  triangle = tri()
  for l,i in langs
    s = left:"#{triangle[i].x + $("#control").width() * 0.5}px", top: "#{triangle[i].y}px"
    if i isnt 2 then s.transform = "translate(-50%, -150%)"
    $("##{l.english}").css s
  for k in Object.keys tracks
    tracks[k].position = new v2(newtrioffset.left + tracks[k].position.x - lasttrioffset.x, newtrioffset.top + tracks[k].position.y - lasttrioffset.y)
    $("##{tracks[k].name}_knob").css({left:tracks[k].position.x, top:tracks[k].position.y})

  lasttrioffset = new v2(newtrioffset.left, newtrioffset.top)
    #get set  offset position, change knobs

switchinfolang = (l) ->
  for i in langs
    $("##{i.english}").html i[l]

initinfo = () ->
  triangle = tri()
  for l, i in langs
    linfo = $("<div>", {class:"info-text", id : l.english, html:l.english})
    linfo.mouseenter( (e) -> switchinfolang(e.currentTarget.id))
    $("#info").append linfo

# fakedrag = false
anyplaying = (stopped = false) ->
  r = []
  for c in clips
    if (if stopped then not tracks[c].playing else tracks[c].playing) then r.push tracks[c].name
  r 

fakecursor = 
  p : new v2(0,0)
  nexttime : 0
  fading : 0
  self : () -> $("#fakecursor")
  active : false
  getdelay : () -> 500 + 3000 * Math.random()
  click : (f) ->
    revert = () -> 
      @self().animate({"border-radius" : "50%"}, 200)
    @self().animate({"border-radius" : "10%"}, 200, revert)
    f()
  move : (x,y, f = null) ->
    pp = @self().position()
    d = dist(new v2(x,y), new v2(pp.left, pp.top))
    @self().animate( {left:x, top:y}, 400 + Math.random() * 1000, randomease(), f)
  moverandom : () -> @move(window.innerWidth * Math.random(), window.innerHeight * Math.random())
  moveontri : () -> 
    c = new v2(window.innerWidth * 0.5, window.innerHeight * 0.5)
    r = Math.random() * ($("#control").width() * 0.5)
    pp = new v2(c.x + r * Math.sin(2*Math.PI * Math.random()), c.y + r * Math.cos(2*Math.PI * Math.random()))
    tr = $("#control").offset()
    tp = tri_clamp(new v2(tr.left, tr.top), 260, 210, pp)
    @move(tp.x, tp.y)
  toggle : (i) ->
    pp = $("##{i}").offset()
    pp.left += $("##{i}").width() * 0.5 + random() * $("##{i}").height() * 0.25
    pp.top += $("##{i}").height() * 0.5 + random() * $("##{i}").height() * 0.25
    click = () -> 
      revert = () -> 
        fakecursor.self().animate({"border-radius" : "50%"}, 200)

        tracks[i].toggle(i)
      fakecursor.self().animate({"border-radius" : "10%"}, 200, revert)
    @move(pp.left, pp.top, click)
  togglerandom : () ->
    i = clips[Math.round(Math.random() * (clips.length - 1))]
    @toggle(randomfrom clips)
  faderandom : () ->
    any = anyplaying()
    if any.length isnt 0 
      n = randomfrom(any)
      @p = new v2($("##{n}_knob").offset().left + $("##{n}_knob").width() * 0.5, $("##{n}_knob").offset().top + $("##{n}_knob").height() * 0.5)
      click = () ->
        revert = () -> 
          grabknob("##{n}_knob", n)
          fakecursor.moveontri()
        fakecursor.self().animate({"border-radius" : "10%"}, 200, revert)
      @move(@p.x, @p.y, click)
      @fading = 1
    else
      @togglerandom()
  activate : () ->
    console.log "starting fake cursor"
    @p.x = mousepos.x
    @p.y = mousepos.y
    @self().css {left:@p.x,top:@p.y}
    @self().animate({ opacity : 1.0}, 1000, (() -> fakecursor.self().css("display", "block")))
    @active = true
    # $("body, video, div").css { cursor : "none"}
  hide : () ->
    console.log "fake cursor stopped"
    if current isnt undefined 
      dropknob()
      @self().animate({"border-radius" : "50%"}, 200)
      @fading = 0
    @self().animate({"opacity" : 0.0}, 1000, (() -> fakecursor.self().css("display", "none")))
    @active = false
  update : () ->
    @p.x = @self().offset().left + @self().width() * 0.5
    @p.y = @self().offset().top + @self().height() * 0.5
    if @fading > 0
      dragknob(@p.x, @p.y)
    if Date.now() > @nexttime and not @self().is(':animated')
      @nexttime = Date.now() + @getdelay()
      r = Math.round(Math.random() * 2)
      if @fading > 0
        switch r
          when 2
            if @fading > 1
              dropknob()
              @self().animate({"border-radius" : "50%"}, 200)
              @fading = 0
          else 
            @fading++
            @moveontri()
      else
        rr = Math.random()
        if rr < 0.5
          # playing = anyplaying()
          # if playing.length < 2 then @toggle(randomfrom(anyplaying(true)))
          # else if playing.length > clips.length * 0.5 then @toggle(randomfrom playing)
          # else @togglerandom()
          @togglerandom()
        else
          @faderandom()

main = () ->
  if autopilot_allowed and lastuseraction + fakecursortimeout < Date.now() and not fakecursor.active
    fakecursor.activate()
  if fakecursor.active then fakecursor.update()
  window.requestAnimationFrame main


loaderknoblang = 0
loading = () ->
  loaded = loadingpercent()
  if loaded is 1
    clips.push "nothing"
    for c in clips
      tracks[c].pick_pos()
      tracks[c].video.get(0).currentTime = 0
      tracks[c].video.get(0).pause()
      for p in tracks[c].sounds.players
        p.get(0).currentTime = 0
        p.get(0).pause()
      tracks[c].sounds.setvolumes(getvolumes(tracks[c].position))
    clips.pop()
    $("#top, #bottom").animate {opacity : 1.0}, 2000
    $("#loadpercent, .knob, #loading, #info").animate( {opacity : 0.0}, 1000, (()->$("#loadpercent, .knob, #loading").remove()))
    lastuseraction = Date.now()
    main()
  else
    tr = tri()
    loaderknoblang = (loaderknoblang+1) % 3
    for i in [0..2]
      $("#loaderknob#{i}").animate({left:tr[(loaderknoblang+i) % 3].x + $("#control").width() * 0.5, top:tr[(loaderknoblang+i) % 3].y},
        500, 
        "linear", 
        () ->
          $("#loading").html "#{langs[loaderknoblang].loading}"
          $("#loadpercent").html "#{Math.floor(100 * loaded)}%"
      )
    window.setTimeout(loading, 1000)


init = () ->
  $(".knob").css {"border-radius": "50%", left:window.innerWidth*0.5, top:window.innerHeight*0.5}

  top = $("<div>", {id:"top", class:"videolane"})
  bottom = $("<div>", {id:"bottom", class:"videolane"})
  $("body").append [top, bottom]
  $("#top, #bottom").css {opacity : 0.0}
  for n, i in clips
    l = clips.length
    x = if i < l / 2 then i * (100 / (l / 2)) else (i - (l / 2)) * (100 / (l / 2))
    w = 20
    p = if i < l / 2 then "#top" else "#bottom"
    tracks[n] = new track(n, x, w, p)

  tracks.nothing = new track("nothing",0,100,"body")
  
  initinfo()
  $(window).resize(resize)
  $("html").mousemove((e) -> 
                      mousepos = new v2(e.clientX, e.clientY)
                      lastuseraction = Date.now()
                      if fakecursor.active
                        fakecursor.hide()
                      else
                        dragknob(e.clientX, e.clientY)
                      )
  $("*").mouseup(() -> 
                 $("##{current}_knob").removeClass("dragged")
                 current = undefined)
  lasttrioffset = $("#control").offset()
  resize()

  clips.push "nothing"
  for c in clips
    loadedfiles[c] = false
    for l in langs
      loadedfiles[c+"_"+l.english[0]] = false
  clips.pop()
  loading()

init()
